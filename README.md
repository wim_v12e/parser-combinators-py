# Name: `parser.combinators`

A library of building blocks for parsing text, written in Python.

# Synopsis

    from parser.combinators import *

A parser is a combination of the parser building blocks from `parser.combinators`.

Most common usage:

    parse_tree = run( parser, str )

Or slightly lower-level:

    (status, rest, matches) = apply( parser, str )
    print( parse_tree )

# Description

`parser.combinators` is a library of parser building blocks ('parser combinators'), inspired by the [Parsec parser combinator library of Haskell](http://legacy.cs.uu.nl/daan/download/parsec/parsec.html).
The idea is that you build a parsers not by specifying a grammar (as in yacc/lex or Parse::RecDescent), but by combining a set of small parsers that parse well-defined items.

There is already [a Python version of Parsec](https://pythonhosted.org/parsec/), the main difference with this library is that the basic mechanism to combine the parsers is a list.

## Usage

Each parser in this library , e.g. `word` or `symbol`, is a function that returns a function (actually, a closure) that parses a string. You can combine these parsers by using special
parsers like `sequence` and `choice`. For example, a JavaScript variable declaration

     var res = 42;

could be parsed as:

    p = [
            symbol('var'),
            word,
            symbol('='),
            natural,
            semi
        ]

which would return

    ['var', 'res', '=', '42']

If you wanted to express that the assignment is optional, i.e. ` var res;` is also valid, you can use `maybe()`:

    p =
         [
            symbol('var'),
            word,
            maybe(
                 [
                   symbol('='),
                   natural
                   ]
            ),
            semi
        ]

If you want to parse alternatives you can use `choice()`. For example, to express that either of the next two lines are valid:

    42
    return(42)

you can write

    p = choice( number, [ symbol('return'), parens( number ) ] )

This example also illustrates the `parens()` parser to parse anything enclosed in parentheses.

## Provided Parsers

The library is not complete in the sense that not all Parsec combinators have been implemented. Currently, it contains:

        whiteSpace : parses any white space, always returns success.
        char( ch ) : parses the character _ch_, does not ignore whitespce

* Lexeme parsers (they remove trailing whitespace):

        identifier: parses any string of lowercase characters, digits and underscores, starting with a character
        word: parses any string of characters, digits and underscores, starting with a character
        natural: parses a positive integer
        number: parses any number
        symbol( s ) : parses a given symbol _s_, e.g. symbol('int')

        punctuation( ch ) : parses _ch_ as a punctuation character
        comma : parses a comma
        semi : parses a semicolon
        colon : parses a colon
        dot : parses a dot

        parseChar( ch ) : parses a given character _ch_

* Combinators:

        sequence( [ parser1, parser2, ... ] ) : the main combinator, to create a sequence of parsers. You can simply provide the list, the `sequence()` function is optional.

        choice( parser1, parser2, ...) : tries the specified parsers in order
        tryParse : normally, the parser consums matching input. try() stops a parser from consuming the string
        maybe : is like tryParse() but always reports success

        enclosedBy( open_char, close_char, parser ): parses 'open_char', then applies `parser`, then 'close_char'
        parens( parser ) : parses '(', then applies $parser, then ')'
        braces( parser ) : parses '{', then applies $parser, then '}'
        brackets( parser ) : parses '[', then applies $parser, then ']'
        angleBrackets( parser ) : parses '<', then applies $parser, then '>'

        many( parser) : applies $parser zero or more times
        many1( parser) : applies $parser one or more times
        sepBy( separator, parser) : parses a list of `parser` separated by `separator`, also a parser
        sepByChar( separator, parser) : parses a list of `parser` separated by `separator`, a character
        commaSep( parser ): parses a comma-separated list of `parser`
        oneOf( [patt1, patt2,...]): like symbol() but parses the patterns in order

        upto( lit_str ) : parses anything up to the first occurrence of a given literal string
        greedyUpto( lit_str ) : parses anything up to the last occurrence of a given literal string

* Dangerous: the `regex` parser takes a regular expression, so you can mix regexes and other combinators ...                                       
        regex( patt )        

## Labeling

You can label any parser in a sequence using a dictionary, for example:

    type_parser = [
              { 'Type' :	word},
              maybe(parens( choice(
                  { 'Kind' : natural},
  				[
  				    symbol('kind'),
  					symbol('='),
                      { 'Kind' : natural}
  				]
  			)))
  		]

Applying this parser returns a tuple as follows:

    tstr = 'integer(kind=8), '
    [status, rest, matches] = apply( type_parser, tstr )

Here,`status` is 0 if the match failed, 1 if it succeeded. `rest` contains the rest of the string.
The actual matches are stored in the array $matches. As every parser returns its results as a list,
`matches` contains the concrete parsed syntax, i.e. a nested array of arrays of strings.

    print(matches) # => [{'Type': 'integer'}, ['kind', '=', {'Kind': '8'}]]

You can remove the unlabeled matches and transform the lists of pairs into maps using `getParseTree` (also conveniently called `run`):

    parse_tree = getParseTree matches

    print(parse_tree) # => [{'Type': 'integer'}, {'Kind': '8'}]

## A more complete example: simple JSON parser

A JSON value is one of text, number, mapping, and collection; mappings are dicts from strings to JSON values; collections are lists of JSON values.

This parser is recursive, so we have to define the recursive parsers using functions because otherwise Python complains that they are not yet defined.

      json_number = {'JNum': number}
      json_text = {'JText': [char("'"),  regex("[^\']+") ,char("'")] }
      json_key = { 'JKey' : [ char("'"), word , char("'") ] }

      def json_value():
              return choice( json_list, json_text, json_number, json_mapping  )
      def json_pair():
              return {'JPair' : [  json_key , colon, {'JVal':json_value} ]  }
      def json_mapping():
              return  { 'JMap' : braces( commaSep(  json_pair  ) ) }        
      def json_list():
              return {'JList' : brackets( commaSep( json_value ))}

Applying this parser to a simple JSON example

      json_string = "{'a' : 'ten', 'b' : 2.10, 'z' : 3.E-10,'c' : [1.0, [2.0, [3.0, 4]]]}"

      parse_tree = run( json_value, json_string)
      print( 'parse_tree: ',parse_tree )

gives

      parse_tree:  {'JMap': [{'JPair': [{'JKey': ['a']}, {'JVal': {'JText': ['ten']}}]}, {'JPair': [{'JKey': ['b']}, {'JVal': {'JNum': '2.10'}}]}, {'JPair': [{'JKey': ['z']}, {'JVal': {'JNum': '3.E-10'}}]}, {'JPair': [{'JKey': ['c']}, {'JVal': {'JList': [{'JNum': '1.0'}, {'JList': [{'JNum': '2.0'}, {'JList': [{'JNum': '3.0'}, {'JNum': '4'}]}]}]}}]}]}

# Author

Wim Vanderbauwhede <Wim.Vanderbauwhede@mail.be>

# Copyright

Copyright 2017- Wim Vanderbauwhede

# License

This library is free software; see the LICENSE file for details.

# See also

- The original Parsec library: [http://legacy.cs.uu.nl/daan/download/parsec/parsec.html](http://legacy.cs.uu.nl/daan/download/parsec/parsec.html) and [http://hackage.haskell.org/package/parsec](http://hackage.haskell.org/package/parsec)
- The Perl version of this library: [https://github.com/wimvanderbauwhede/Perl-Parser-Combinators](https://github.com/wimvanderbauwhede/Perl-Parser-Combinators) and [https://metacpan.org/module/Parser::Combinators](https://metacpan.org/module/Parser::Combinators)

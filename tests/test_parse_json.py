from parser.combinators import *

# simple JSON parser
# a json value is one of text, number, mapping, and collection
# mappings are dicts from strings to json values; collections are lists of json values

json_number = {'JNum': number}
json_text = {'JText': [char("'"),  regex("[^\']+") ,char("'")] }
json_key = { 'JKey' : [ char("'"), word , char("'") ] }

def json_value():
        return choice( json_list, json_text, json_number, json_mapping  )
def json_pair():
        return {'JPair' : [  json_key , colon, {'JVal':json_value} ]  }
def json_mapping():
        return  { 'JMap' : braces( commaSep(  json_pair  ) ) }        
def json_list():
        return {'JList' : brackets( commaSep( json_value ))}

json_string = "{'a' : 'ten', 'b' : 2.10, 'z' : 3.E-10,'c' : [1.0, [2.0, [3.0, 4]]]}"

parse_tree = run( json_value, json_string)
print( 'parse_tree: ',parse_tree )


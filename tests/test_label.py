from parser.combinators import *

type_parser = [
            { 'Type' :	word},
            maybe(parens( choice(
                { 'Kind' : natural},
				[
				    symbol('kind'),
					symbol('='),
                    { 'Kind' : natural}
				]
			)))
		]

# Applying this parser returns a tuple as follows:

tstr = 'integer(kind=8), '
[status, rest, matches] = apply( type_parser, tstr )

#Here,`status` is 0 if the match failed, 1 if it succeeded.  `rest` contains the rest of the string.
#The actual matches are stored in the array $matches. As every parser returns its resuls as an array ref,
#`matches` contains the concrete parsed syntax, i.e. a nested array of arrays of strings.

print(matches) # => [{'Type' => 'integer'},['kind','\\=',{'Kind' => '8'}]]
parse_tree = getParseTree( matches )

print(parse_tree) # => {Type : 'integer',Kind : 8
